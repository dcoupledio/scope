<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Scope\ScopeFactory;
use Decoupled\Core\Scope\Test\Mock;
use Decoupled\Core\Scope\Proxy;

class ScopeTest extends TestCase{

    public function testCanUseParentScope()
    {
        $factory = new ScopeFactory();

        $scope = $factory->make();

        $parent = $factory->make([ 'test' => 1 ]);

        $scope->setParent( $parent );

        $this->assertEquals( $scope['test'], 1 );

        return $scope;
    }

    /**
     * @depends testCanUseParentScope
     */

    public function testCanUseStringableCallback( $scope )
    {
        $scope['callback'] = function(){

            return $this->offsetGet('test');
        };

        $this->assertEquals( (string) $scope['callback'], $scope['test'] );

        return $scope;
    }

    /**
     * @depends testCanUseStringableCallback
     */

    public function testCanUseProxyMethods( $scope )
    {
        $scope['mock'] = new Mock();

        $this->assertInstanceOf( Proxy::class, $scope['mock'] );

        $scope['mock']->prop = 2;

        $this->assertEquals( 
            $scope['mock']->getProp(), 
            $scope['mock']->prop 
        );

        $this->assertEquals( (string) $scope['mock'], 2 );
    }

}