<?php namespace Decoupled\Core\Scope\Test;

class Mock{

    public $prop = 1;

    public function __invoke()
    {
        return (string) $this->getProp();
    }

    public function getProp()
    {
        return $this->prop;
    }

}