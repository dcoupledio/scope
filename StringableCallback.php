<?php namespace Decoupled\Core\Scope;

class StringableCallback extends Proxy{

    public function __construct( callable $callback )
    {
        $this->setElement( $callback );
    }

    public function __toString()
    {
        $element = $this->getElement();

        if( !is_array($element) && method_exists( $element, '__toString') )
        {
            return (string) $element;
        }

        return (string) call_user_func( $element );
    }

}