<?php namespace Decoupled\Core\Scope;

interface ProxyInterface{

    public function __toString();

    public function __get( $property );

    public function __set( $property, $value );

    public function __call( $method, $params );

    public function __invoke();

    public function setElement( $object );

    public function getElement();
}