<?php namespace Decoupled\Core\Scope;

use Closure;
use Illuminate\Support\Collection;

class Scope extends Collection{

    protected $parentScope;

    public function __construct( array $items = [] )
    {
        foreach( $items as $key => $v )
        {
            $this[$key] = $v;
        }        
    }

    public function setParent( Scope $scope )
    {
        $this->parentScope = $scope;

        return $this;
    }

    public function getParent()
    {
        return $this->parentScope;
    }

    public function offsetGet( $key )
    {
        if( !isset($this[$key]) )
        {
            return $this->parentScope[$key];
        }

        return parent::offsetGet( $key );
    }

    public function offsetSet( $key, $value )
    {
        if( is_callable($value) )
        {
            $value = $this->makeStringableCallback( $value );
        }

        parent::offsetSet( $key, $value );
    }

    public function makeStringableCallback( Callable $callback )
    {
        if( $callback instanceof \Closure ) 
        {
            $callback = $callback->bindTo( $this );
        }

        return new StringableCallback( $callback );
    }
}