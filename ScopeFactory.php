<?php namespace Decoupled\Core\Scope;

class ScopeFactory implements ScopeFactoryInterface{

    public function make( array $properties = [] )
    {
        $scope = new Scope( $properties );

        return $scope;
    }
}