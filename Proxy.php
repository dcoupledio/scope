<?php namespace Decoupled\Core\Scope;

class Proxy implements ProxyInterface{

    protected $element;

    public function __toString()
    {
        return (string) $this->getElement();
    }

    public function __get( $property )
    {
        return $this->getElement()->{$property};
    }

    public function __set( $property, $value )
    {
        return $this->getElement()->{$property} = $value;
    }

    public function __call( $method, $params )
    {
        $element = $this->getElement();

        return call_user_func_array([ $element, $method ], $params);   
    }

    public function __invoke()
    {
        $element = $this->getElement();

        return $element();        
    }

    public function setElement( $object )
    {
        $this->element = $object;

        return $this;
    }

    public function getElement()
    {
        return $this->element;
    }
}