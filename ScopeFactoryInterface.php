<?php namespace Decoupled\Core\Scope;

interface ScopeFactoryInterface{

    public function make( array $properties = [] );
}